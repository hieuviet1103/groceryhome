﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GroceryHome.Areas.Models
    {
    public class QuanLyGioHang
    {

    }

    public class SanPhamDaDat
    {
        public int Id { get; set; }
        public int IdSanPham { get; set; }
        public int IdGioHang { get; set; }
        public string TenSP { get; set; }
        public Nullable<int> SoLuong { get; set; }
        public Nullable<float> DonGia { get; set; }
        public Nullable<float> ThanhTien { get; set; }
        public Nullable<System.DateTime> NgayThem { get; set; }
        public Nullable<int> IsDelete { get; set; }
        public bool IsAccept { get; set; }
    }

    public class ThongTinDatHang
    {
        public ThongTinDatHang()
        {

        }
        public int IdGioHang { get; set; }
        public string HoVaTen { get; set; }
        public string DienThoai { get; set; }
        public string Tinh { get; set; }
        public string Huyen { get; set; }
        public string DiaChi { get; set; }
        public string GhiChu { get; set; }

        public string ThongBao { get; set; }
        
    }

    public class DetailsDonHang
    {
        public DetailsDonHang()
        {

        }
        public int Id { get; set; }
        public int IdSanPham { get; set; }
        public int IdGioHang { get; set; }
        public string TenSP { get; set; }
        public Nullable<int> SoLuong { get; set; }
        public Nullable<int> SoLuongConLai { get; set; }
        public Nullable<decimal> DonGia { get; set; }
        public Nullable<decimal> ThanhTien { get; set; }
        public Nullable<System.DateTime> NgayThem { get; set; }
        public Nullable<int> IsDelete { get; set; }
        public bool IsAccept { get; set; }

    }
}