﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GroceryHome.Models;

namespace GroceryHome.Areas.DieuHanh.Controllers
{
    public class QuanLyDonHangController : Controller
    {

        private readonly GroceryHomeEntities dB = new GroceryHomeEntities();
        // GET: DieuHanh/QuanLyDonHang
        public ActionResult Index()
        {
            if (Session["CurrentUser"] == null)
            {
                return RedirectToAction("Login", "Login");
            }
            List<tbl_DatHang> ListDatHang = new List<tbl_DatHang>();
            try
            {
                ListDatHang = dB.tbl_DatHang.Where(d => d.IsDelete == false).ToList();
            }
            catch
            {


            }
            return View(ListDatHang);
        }

        public ActionResult DetailsDonHang(string id)
        {
            Guid Id = new Guid(id);
            ViewBag.IdDonHang = id;
            var IdGioHang = dB.tbl_DatHang.Find(Id).IdGioHang;
            var listSanPham = dB.tbl_GioHang_SanPham.Where(d => d.IdGioHang == IdGioHang && d.IsDelete == 0).ToList();
            var models = new List<DetailsDonHang>();
            foreach (var item in listSanPham)
            {
                if (models.Any(d => d.IdSanPham == item.IdSanPham))
                {
                    models.Where(d => d.IdSanPham == item.IdSanPham).FirstOrDefault().SoLuong += item.SoLuong;
                }
                else
                {
                    var SP = new DetailsDonHang();
                    SP.Id = item.Id;
                    SP.IdGioHang = item.IdGioHang;
                    SP.IdSanPham = item.IdSanPham;
                    SP.SoLuong = item.SoLuong;
                    SP.SoLuongConLai = dB.tbl_SanPham.Find(item.IdSanPham).SoLuongConLai;
                    SP.DonGia = item.DonGia;
                    SP.ThanhTien = item.ThanhTien;
                    SP.TenSP = item.TenSP;
                    SP.NgayThem = item.NgayThem;
                    SP.IsDelete = item.IsDelete;
                    SP.Note = item.Note;
                    models.Add(SP);
                }
            }
            return View(models);
        }

        public void ConfirmDonHang(string id, string status)
        {
            Guid Id = new Guid(id);
            var donHang = dB.tbl_DatHang.Find(Id);
            var SPGioHang = dB.tbl_GioHang_SanPham.Where(d => d.IdGioHang == donHang.IdGioHang).ToList();
            var checkStatus = false;
            foreach (var item in SPGioHang)
            {
                var SP = dB.tbl_SanPham.Find(item.IdSanPham);
                if (item.SoLuong <= SP.SoLuongConLai)
                {
                    SP.SoLuongConLai -= item.SoLuong;
                    item.Note = "Đang chuẩn bị";
                    item.IsAccept = 1;
                    checkStatus = true;                  
                }
                else
                {
                    item.IsAccept = 2;
                    donHang.TongHoaDon -= item.ThanhTien;
                    item.ThanhTien = 0;
                    item.Note = "Hết hàng";
                    donHang.AdminFeedback += item.TenSP + " không đủ hàng" + "\r\n";
                }
                dB.SaveChanges();
            }
            if (checkStatus)
            {
                donHang.Status = 1;
            }
            else
            {
                donHang.Status = 2;
            }
            dB.SaveChanges();
        }

        public void RejectDonHang(string id, string status)
        {
            Guid Id = new Guid(id);
            var donHang = dB.tbl_DatHang.Find(Id);
            donHang.Status = 2;
            dB.SaveChanges();
        }

        public ActionResult DonChoDuyet()
        {
            if (Session["CurrentUser"] == null)
            {
                return RedirectToAction("Login", "Login");
            }
            List<tbl_DatHang> ListDatHang = new List<tbl_DatHang>();
            try
            {
                ListDatHang = dB.tbl_DatHang.Where(d => d.IsDelete == false && d.Status == 0).ToList();
            }
            catch
            {


            }
            return View(ListDatHang);
        }

        public ActionResult DonDaHuy()
        {
            if (Session["CurrentUser"] == null)
            {
                return RedirectToAction("Login", "Login");
            }
            List<tbl_DatHang> ListDatHang = new List<tbl_DatHang>();
            try
            {
                ListDatHang = dB.tbl_DatHang.Where(d => d.IsDelete == false && d.Status == 2).ToList();
            }
            catch
            {


            }
            return View(ListDatHang);

        }

        public ActionResult DaXacNhan()
        {
            if (Session["CurrentUser"] == null)
            {
                return RedirectToAction("Login", "Login");
            }
            List<tbl_DatHang> ListDatHang = new List<tbl_DatHang>();
            try
            {
                ListDatHang = dB.tbl_DatHang.Where(d => d.IsDelete == false && d.Status ==1).ToList();
            }
            catch
            {


            }
            return View(ListDatHang);
        }
    }
}