﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GroceryHome.Models;

namespace GroceryHome.Controllers
{
    public class HomeController : Controller
    {
        private readonly GroceryHomeEntities dB = new GroceryHomeEntities();
        public ActionResult Index()
        {
            if (CustomerLoginStatus.IsLogin)
            {
                var models = dB.tbl_SanPham.Where(d => d.Id != 0).ToList();
                return View(models);
            }
            else
            {
                return RedirectToAction("CustomerLogin", "Account");
            }
        }

        public ActionResult GioHang(ThongTinDatHang ttDatHang)
        {
            ViewBag.ThongTin = ttDatHang;
            List<tbl_GioHang_SanPham> ListSP = new List<tbl_GioHang_SanPham>();
            var models = new List<tbl_GioHang_SanPham>();
            try
            {
                var IdKhachHang = dB.tbl_KhachHang.Where(d => d.UserName == CustomerLoginStatus.CustomerUser).FirstOrDefault().Id;
                var IdGioHang = dB.tbl_GioHang.Where(d => d.IdKhachHang == IdKhachHang && d.IsComplete == false).FirstOrDefault().Id;
                ListSP = dB.tbl_GioHang_SanPham.Where(d => d.IdGioHang == IdGioHang && d.IsDelete == 0).ToList();
                foreach (var item in ListSP)
                {
                    if (models.Any(d=>d.IdSanPham == item.IdSanPham))
                    {
                        models.Where(d => d.IdSanPham == item.IdSanPham).FirstOrDefault().SoLuong += item.SoLuong;
                    }
                    else
                    {
                        models.Add(item);
                    }
                }
            }
            catch { }
            return View(models);
        }

        public ActionResult DonHang()
        {
            List<tbl_DatHang> ListDatHang = new List<tbl_DatHang>();
            try
            {
                var IdKhachHang = CustomerLoginStatus.CustomerUserId;
                ListDatHang = dB.tbl_DatHang.Where(d => d.IdKhachHang == IdKhachHang && d.IsDelete == false).ToList();
            }
            catch
            {


            }
            return View(ListDatHang);
        }

        public ActionResult DetailsDonHang(string id)
        {
            Guid Id = new Guid(id);
            var IdGioHang = dB.tbl_DatHang.Find(Id).IdGioHang;
            var listSanPham = dB.tbl_GioHang_SanPham.Where(d => d.IdGioHang == IdGioHang && d.IsDelete == 0).ToList();
            var models = new List<DetailsDonHang>();
            foreach (var item in listSanPham)
            {
                if (models.Any(d=>d.IdSanPham == item.IdSanPham))
                {
                    models.Where(d => d.IdSanPham == item.IdSanPham).FirstOrDefault().SoLuong += item.SoLuong;                    
                }
                else
                {
                    var SP = new DetailsDonHang();
                    SP.Id = item.Id;
                    SP.IdGioHang = item.IdGioHang;
                    SP.IdSanPham = item.IdSanPham;
                    SP.SoLuong = item.SoLuong;
                    SP.SoLuongConLai = dB.tbl_SanPham.Find(item.IdSanPham).SoLuongConLai;
                    SP.DonGia = item.DonGia;
                    SP.ThanhTien = item.ThanhTien;
                    SP.TenSP = item.TenSP;
                    SP.NgayThem = item.NgayThem;
                    SP.IsDelete = item.IsDelete;
                    SP.Note = item.Note;
                    models.Add(SP);
                }
            }
            return View(models);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult AddSpToGioHang(string id, string count)
        {
            if (CustomerLoginStatus.IsLogin)
            {
                try
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        int Id = Convert.ToInt32(id);
                        int Count;
                        Int32.TryParse(count, out Count);
                        if (CustomerLoginStatus.IsLogin)
                        {
                            int idKhachHang = Convert.ToInt32(dB.tbl_KhachHang.Where(d => d.UserName == CustomerLoginStatus.CustomerUser).FirstOrDefault().Id);

                            if (!dB.tbl_GioHang.Any(d => d.IdKhachHang == idKhachHang && d.IsComplete == false))
                            {
                                //Add Giỏ hàng
                                tbl_GioHang newGioHang = new tbl_GioHang();
                                newGioHang.IdKhachHang = idKhachHang;
                                newGioHang.NgayTao = DateTime.Now;
                                newGioHang.IsDelete = false;
                                newGioHang.IsComplete = false;
                                dB.tbl_GioHang.Add(newGioHang);
                                dB.SaveChanges();

                                //Add sản phẩm vào giỏ
                                tbl_GioHang_SanPham newSanPham = new tbl_GioHang_SanPham();
                                var IdGioHang = dB.tbl_GioHang.Where(d => d.IdKhachHang == idKhachHang && d.IsComplete == false ).FirstOrDefault().Id;
                                var SanPham = dB.tbl_SanPham.Find(Id);
                                newSanPham.IdGioHang = IdGioHang;
                                newSanPham.IdSanPham = Convert.ToInt32(Id);
                                newSanPham.TenSP = SanPham.TenSP;
                                newSanPham.SoLuong = Count != 0 ? Count : 1;
                                newSanPham.DonGia = SanPham.Gia;
                                newSanPham.ThanhTien = newSanPham.DonGia * newSanPham.SoLuong;
                                newSanPham.NgayThem = DateTime.Now;
                                newSanPham.IsDelete = 0;
                                dB.tbl_GioHang_SanPham.Add(newSanPham);
                                dB.SaveChanges();
                            }

                            else
                            {
                                //Add sản phẩm vào giỏ
                                tbl_GioHang_SanPham newSanPham = new tbl_GioHang_SanPham();
                                var IdGioHang = dB.tbl_GioHang.Where(d => d.IdKhachHang == idKhachHang && d.IsComplete == false).FirstOrDefault().Id;
                                var SanPham = dB.tbl_SanPham.Find(Id);
                                newSanPham.IdGioHang = IdGioHang;
                                newSanPham.IdSanPham = Convert.ToInt32(Id);
                                newSanPham.TenSP = SanPham.TenSP;
                                newSanPham.SoLuong = Count != 0 ? Count : 1;
                                newSanPham.DonGia = SanPham.Gia;
                                newSanPham.ThanhTien = newSanPham.DonGia * newSanPham.SoLuong;
                                newSanPham.NgayThem = DateTime.Now;
                                newSanPham.IsAccept = 0;
                                newSanPham.IsDelete = 0;
                                dB.tbl_GioHang_SanPham.Add(newSanPham);
                                dB.SaveChanges();

                            }
                        }
                    }
                    return RedirectToAction("Index");
                }
                catch
                {
                    Session["IsAddSanPham"] = "Không thể thêm sản phẩm vào giỏ";
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("CustomerLogin", "Account");
            }
        }

        public void XoaSPGioHang(string id)
        {
            int Id = Convert.ToInt32(id);
            var delSPGioHang = dB.tbl_GioHang_SanPham.Find(Id);
            delSPGioHang.IsDelete = 1;
            dB.SaveChanges();
        }
        public ActionResult DatHang(ThongTinDatHang ttDatHang)
        {
            if (Request.HttpMethod == "POST")
            {
                int temp;
                if (string.IsNullOrEmpty(ttDatHang.HoVaTen))
                {
                    ttDatHang.ThongBao = "*Họ và tên không để trống";
                    return RedirectToAction("GioHang", ttDatHang);
                }
                else if (string.IsNullOrEmpty(ttDatHang.DienThoai) || (ttDatHang.DienThoai.Length > 10) || (Int32.TryParse(ttDatHang.DienThoai, out temp) == false))
                {
                    ttDatHang.ThongBao = "*Yêu cầu nhập đúng số điện thoại";
                    return RedirectToAction("GioHang", ttDatHang);
                }
                else if (string.IsNullOrEmpty(ttDatHang.Tinh))
                {
                    ttDatHang.ThongBao = "*Tỉnh không để trống";
                    return RedirectToAction("GioHang", ttDatHang);
                }
                else if (string.IsNullOrEmpty(ttDatHang.Huyen))
                {
                    ttDatHang.ThongBao = "*Huyện không để trống";
                    return RedirectToAction("GioHang", ttDatHang);
                }
                else if (string.IsNullOrEmpty(ttDatHang.DiaChi))
                {
                    ttDatHang.ThongBao = "*Địa chỉ không để trống";
                    return RedirectToAction("GioHang", ttDatHang);
                }
                else
                {
                }
                //int Id = Convert.ToInt32(id);
                int Id = ttDatHang.IdGioHang;
                var CurrentUser = dB.tbl_KhachHang.Find(CustomerLoginStatus.CustomerUserId);
                var GioHang = dB.tbl_GioHang.Find(Id);
                var SanPham = dB.tbl_GioHang_SanPham.Where(d => d.IdGioHang == Id && d.IsDelete == 0).ToList();
                decimal? TongTien = 0;
                foreach (var item in SanPham)
                {
                    TongTien += item.ThanhTien;
                }
                tbl_DatHang datHang = new tbl_DatHang();
                datHang.Id = Guid.NewGuid();
                datHang.IdGioHang = Id;
                datHang.IdKhachHang = CustomerLoginStatus.CustomerUserId;
                datHang.TenKhachHang = ttDatHang.HoVaTen;
                //datHang.DiaChi = CurrentUser.DiaChi;
                datHang.DiaChi = ttDatHang.DiaChi + ", " + ttDatHang.Huyen + ", " + ttDatHang.Tinh;
                datHang.Phone = CurrentUser.PhoneNumber;
                datHang.NgayDatHang = DateTime.Now;
                datHang.TongHoaDon = TongTien;
                datHang.GhiChu = ttDatHang.GhiChu;
                datHang.Status = 0;
                datHang.IsDelete = false;
                dB.tbl_DatHang.Add(datHang);
                GioHang.IsComplete = true;
                dB.SaveChanges();
                ttDatHang.ThongBao = "Đặt hàng thành công";
                return RedirectToAction("GioHang", ttDatHang);
            }
            return RedirectToAction("GioHang", ttDatHang);
        }
    }
}